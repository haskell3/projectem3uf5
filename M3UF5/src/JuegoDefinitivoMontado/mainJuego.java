package JuegoDefinitivoMontado;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Random;

import AbstractFactory.AlaDelta;
import AbstractFactory.Bestia;
import AbstractFactory.CofreTresor;
import AbstractFactory.Cotxe;
import AbstractFactory.Diamant;
import AbstractFactory.Enemic;
import AbstractFactory.EnemicFactory;
import AbstractFactory.Espasa;
import AbstractFactory.FactoryProvider;
import AbstractFactory.Hamburguesa;
import AbstractFactory.Laxant;
import AbstractFactory.Monedes;
import AbstractFactory.ObjecteBonus;
import AbstractFactory.ObjecteBonusFactory;
import AbstractFactory.ObjecteMillorar;
import AbstractFactory.ObjecteMillorarFactory;
import AbstractFactory.ObjectesCacar;
import AbstractFactory.ObjectesCacarFactory;
import AbstractFactory.Pirata;
import AbstractFactory.Vehicles;
import AbstractFactory.VehiclesFactory;
import Decorator.Color;
import Decorator.ColorJugadorDecorator;
import Decorator.ObjectesMillorarDecorator;
import Decorator.VehiclesDecorator;
import FactoryMethod.Jugador;
import FactoryMethod.JugadorComerciant;
import FactoryMethod.JugadorFactory;
import FactoryMethod.JugadorGuerrer;
import FactoryMethod.JugadorMag;
import FactoryMethod.TipusJugador;
import jdbc.ComparatorPunts;
import jdbc.JugadorComparable;
import observer.ColectorPunts;
import observer.ObjecteGuanyat;
import observer.TipusJugadorFactoryMethod;

public class mainJuego {
	public static Random r = new Random();
	
	public static void main(String[] args) {
		
		FuncionesJDBC jdbc = new FuncionesJDBC();
		jdbc.newConnection();
		
		//1-Creacion de las factories
		JugadorFactory fj = new JugadorFactory();
		ObjecteBonusFactory fob = (ObjecteBonusFactory) FactoryProvider.getFactory("Objectes bonus");
		ObjectesCacarFactory foc = (ObjectesCacarFactory) FactoryProvider.getFactory("Objecte a caçar");
		ObjecteMillorarFactory fom = (ObjecteMillorarFactory) FactoryProvider.getFactory("Objectes a millorar");
		EnemicFactory fe = (EnemicFactory) FactoryProvider.getFactory("Enemic");
		VehiclesFactory fv = (VehiclesFactory) FactoryProvider.getFactory("Vehicle");
		
		//2-Creacio dels jugadors i afegir a BD
		Jugador asunia = fj.crearJugador("Asunia", 0, TipusJugador.Guerrer);
		Jugador gisela = fj.crearJugador("Gisela", 0, TipusJugador.Comerciant);
		Jugador ari = fj.crearJugador("Ari", 0, TipusJugador.Mag);
		ArrayList<JugadorComparable> jugadorsarray = new ArrayList<JugadorComparable>();
		LinkedList<Jugador> jugadors = new LinkedList<Jugador>();
		jugadorsarray.add(new JugadorComparable(asunia));
		jugadorsarray.add(new JugadorComparable(gisela));
		jugadorsarray.add(new JugadorComparable(ari));
		
		jugadors.add(asunia);
		jugadors.add(gisela);
		jugadors.add(ari);
		
		jdbc.afegirJugadorsBD(jugadorsarray);
		
		//3-Crear objetos de cada clase (Eloi porque nos haces esto)
		ArrayList<Vehicles> vehicles = new ArrayList<Vehicles>();
		Vehicles aladelta = fv.create("La fiumba", "Ala delta");
		Vehicles cotxe = fv.create("Mercedes", "Cotxe");
		vehicles.add(cotxe);
		vehicles.add(aladelta);
		
		ArrayList<Enemic> enemics = new ArrayList<Enemic>();
		Enemic bestia = fe.create("PutoFurro", "Bestia");
		Enemic pirata = fe.create("Paquito", "Pirata");
		enemics.add(bestia);
		enemics.add(pirata);
		
		ArrayList<ObjecteMillorar> objectesmillorar = new ArrayList<ObjecteMillorar>();
		ObjecteMillorar espasa = fom.create("Excalibur", "Espasa");
		ObjecteMillorar laxant = fom.create("Me cago", "Laxant");
		objectesmillorar.add(espasa);
		objectesmillorar.add(laxant);
		
		asunia = new ObjectesMillorarDecorator(asunia, espasa);
		gisela = new ObjectesMillorarDecorator(gisela, espasa);
		ari = new ObjectesMillorarDecorator(ari, espasa);
		
		ArrayList<ObjecteBonus> objectesbonus = new ArrayList<ObjecteBonus>();
		ObjecteBonus hamburguesa = fob.create("Burguer Cangre Burguer", "Hamburguesa");
		ObjecteBonus diamant = fob.create("Diamant blau", "Diamant");
		objectesbonus.add(hamburguesa);
		objectesbonus.add(diamant);
		
		ArrayList<ObjectesCacar> objectescacar = new ArrayList<ObjectesCacar>();
		ObjectesCacar monedes = foc.create("Euro", "Monedes");
		ObjectesCacar cofretresor = foc.create("Cofre Epic", "Cofre del tresor");
		objectescacar.add(monedes);
		objectescacar.add(cofretresor);
		
		ArrayList<Color> colors = new ArrayList<Color>();
		colors.add(Color.AQUA);
		colors.add(Color.HOTPINK);
		colors.add(Color.LIME);
		
		//4-Crear el observable y suscribir a los jugadores
		ColectorPunts cPunts = new ColectorPunts();
		TipusJugadorFactoryMethod asuniaObserver = new TipusJugadorFactoryMethod(asunia.getNom(), 0, cPunts);
		TipusJugadorFactoryMethod giselaObserver = new TipusJugadorFactoryMethod(gisela.getNom(), 0, cPunts);
		TipusJugadorFactoryMethod ariObserver = new TipusJugadorFactoryMethod(ari.getNom(), 0, cPunts);
		
		//5- Cosa juego
		for (int i = 0; i < 1000; ++i) {
			int dau = r.nextInt(0,6);
			for (int j = 0; j < dau; ++j) {
				jugadors.addLast(jugadors.poll());
			}
			
			int switchNumber = r.nextInt(0, 100)%6;
			Jugador jugador = jugadors.removeFirst();
			switch (switchNumber) {
			case 0:
				int n = r.nextInt(2);
				ObjecteGuanyat o = new ObjecteGuanyat(jugador, objectescacar.get(n));
				System.out.println(jugador.getNom() + " ha caçat une " + objectescacar.get(n));
				
				
				cPunts.cambienPuntuaciones(o);
				
				
				break;
			case 1:
				int n1 = r.nextInt(2);
				ObjecteGuanyat o1 = new ObjecteGuanyat(jugador, objectesbonus.get(n1));
				System.out.println(jugador.getNom() + " ha aconseguit une " + objectesbonus.get(n1));
				
				cPunts.cambienPuntuaciones(o1);
							
				break;
			case 2:
				int n2 = r.nextInt(2);
				ObjecteGuanyat o2 = new ObjecteGuanyat(jugador, enemics.get(n2));
				System.out.println(jugador.getNom() + " ha sigut atacat per une " + enemics.get(n2));
				
				cPunts.cambienPuntuaciones(o2);
				
				break;
			case 3:
				int n3 = r.nextInt(3);
				jugador = new ColorJugadorDecorator(jugador, colors.get(n3));
				System.out.println(jugador.getNom() + " ara té el color " + colors.get(n3));
				
				break;
			case 4:
				int n4 = r.nextInt(2);
				jugador = new VehiclesDecorator(jugador, vehicles.get(n4));
				System.out.println(jugador.getNom() + " ara pot muntar en une " + vehicles.get(n4));
							
				break;
			case 5:
				int n5 = r.nextInt(2);
				jugador = new ObjectesMillorarDecorator(jugador, objectesmillorar.get(n5));
				System.out.println(jugador.getNom() + " ara té equipat une " + objectesmillorar.get(n5));
				
				break;

			default:
				break;
			}
			System.out.println("asunia observer " + asuniaObserver.getPunts());
			asunia.setPunts(asuniaObserver.getPunts());
			System.out.println("asunia normal " + asunia.getPunts());
			gisela.setPunts(giselaObserver.getPunts());
			ari.setPunts(ariObserver.getPunts());
			
			jugadors.addFirst(jugador);
			
		}
		
		//6- Treure tots els observables
		cPunts.seAcaboElJogo();
		
		//7 y 8 - Cosa de BD final
		System.out.println(asunia.getPunts());
		jugadorsarray.get(0).j = asunia;
		jugadorsarray.get(1).j = gisela;
		jugadorsarray.get(2).j = ari;
		jdbc.ComprobacioFinal(jugadorsarray);	
		
		
		
	}
	
	


}
