package JuegoDefinitivoMontado;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;

import jdbc.ComparatorPunts;
import jdbc.JugadorComparable;

public class FuncionesJDBC {
	static Connection conn = null;
	static Statement stmt = null;
	static ResultSet rs = null;
	
	public void newConnection() {
        try {
         conn =
         DriverManager.getConnection("jdbc:mysql://localhost/videojocjdbc?" + "user=root&password=super3");

        } catch (SQLException ex) {
             // handle any errors
             System.out.println("SQLException: " + ex.getMessage());
             System.out.println("SQLState: " + ex.getSQLState());
             System.out.println("VendorError: " + ex.getErrorCode());
        } 
	}
	
	public void afegirJugadorsBD(ArrayList<JugadorComparable> jugadors) {
		
		try {

			stmt = conn.createStatement();
			
			//Sentencia para quitar el safe mode y poder updatear y deletear sin usar primary key(necesario para el delete por puntos)
			stmt.execute("SET SQL_SAFE_UPDATES = 0;");
			
			for (JugadorComparable jugador : jugadors) {
				if (!IsJugadorInBD(stmt, rs, jugador.j.getNom())) {
					stmt.execute("insert into jugador (nom, saldo, punts) values ('" + jugador.j.getNom() + "', " + jugador.j.getSaldo() + ", " + jugador.j.getPunts() + ")");
				} else {
					System.out.println("El jugador ja està registrat a la BD");
				}
				
			}

			System.out.println("---Jugadors en partida---");
			mostrarJugadors(stmt, rs);
			
		} catch (SQLException ex) {

			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());

		} finally {

			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
				
				} 
				rs = null;
			}
			if (stmt != null) {

				try {

					stmt.close();
				} catch (SQLException sqlEx) {
					
				} 
				stmt = null;
			}
		}
		
	}
	
	public void mostrarJugadors(Statement stmt, ResultSet rs) {
		
		try {
			if(stmt.execute("SELECT * FROM jugador"))
			{
				rs = stmt.getResultSet();
				while(rs.next())
				{
					System.out.println("Nom: " + rs.getString("nom") + ", saldo: " + rs.getDouble("saldo") + ", punts: " + rs.getDouble("punts"));
				}
				System.out.println();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	public boolean IsJugadorInBD(Statement stmt, ResultSet rs, String nom) {
		try {
			if(stmt.execute("SELECT * FROM jugador"))
			{
				rs = stmt.getResultSet();
				while(rs.next())
				{
					if(nom.equals(rs.getString("nom"))) {
						return true;
						
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
		
	}
	
	public void ComprobacioFinal(ArrayList<JugadorComparable> jugadors) {
		
		try {

			stmt = conn.createStatement();
			
			//Sentencia para quitar el safe mode y poder updatear y deletear sin usar primary key(necesario para el delete por puntos)
			for (JugadorComparable jugador : jugadors) {
				//Se que la clave primaria no es el nombre pero la clase jugador no tiene id asique que para mi el nombre es clave primaria :)
				//Al que no le vaya esta sentendia antes tiene que ejecutar esto: SET SQL_SAFE_UPDATES = 0;
				//Es para quitar el safe mode que te impide update or delete de querys que no tengan un where con clave primaria
				//La sentencia es necesaria anque este update se hiciese con primary key debido al delete que viene despues
				//Ya he ejecutado yo la sentencia al principio del codigo pero no borro este comentario por pereza
				
				//Vaya pringado el de los comentarios de arriba ^
				stmt.execute("update jugador set punts = " + jugador.j.getPunts() + " where nom = '" + jugador.j.getNom() + "'");
			}
			stmt.execute("delete from jugador where punts < 500");

			System.out.println("---Jugadors en final de partida---");
			mostrarJugadors(stmt, rs);
			
			System.out.println("Llista ordenada per noms ascendent");
			
			Collections.sort(jugadors);
			System.out.println(jugadors);
			
			System.out.println("Llista ordenada per punts descendents");
			
			Collections.sort(jugadors, new ComparatorPunts());
			System.out.println(jugadors);
			
		} catch (SQLException ex) {

			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());

		} finally {

			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
				
				} 
				rs = null;
			}
			if (stmt != null) {

				try {

					stmt.close();
				} catch (SQLException sqlEx) {
					
				} 
				stmt = null;
			}
		}
		
	}

}
