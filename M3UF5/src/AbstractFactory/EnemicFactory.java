package AbstractFactory;

public class EnemicFactory implements AbstractFactory<Enemic> {

	@Override
	public Enemic create(String nom, String tipus) {
		if (tipus.equalsIgnoreCase("Pirata")) {
			return new Pirata(nom);
		} else if (tipus.equalsIgnoreCase("Monstre")) {
			return new Monstre(nom);
		} else if (tipus.equalsIgnoreCase("Bestia")) {
			return new Bestia(nom);
		} else {
			return null;
		}
	}

}
