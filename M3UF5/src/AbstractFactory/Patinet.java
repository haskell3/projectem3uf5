package AbstractFactory;

public class Patinet implements Vehicles {
	String nom;

	public Patinet(String nom) {
		super();
		this.nom = nom;
	}

	@Override
	public String toString() {
		return "Patinet [nom=" + nom + "]";
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

}
