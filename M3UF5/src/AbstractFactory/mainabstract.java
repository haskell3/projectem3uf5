package AbstractFactory;

public class mainabstract {
	public static void main(String[] args) {
		Monstre m = (Monstre) FactoryProvider.getFactory("Enemic").create("Sauron", "Monstre");
		Cotxe c = (Cotxe) FactoryProvider.getFactory("Vehicle").create("Sandero", "Cotxe");
		RedBull r = (RedBull) FactoryProvider.getFactory("Objectes bonus").create("Red Bull te da alaaas", "Red Bull");
		
		System.out.println(m);
		System.out.println(c);
		System.out.println(r);
	}

}
