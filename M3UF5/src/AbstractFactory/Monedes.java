package AbstractFactory;

import observer.ObjecteGuanyable;

public class Monedes implements ObjectesCacar, ObjecteGuanyable {
	String nom;
	int puntssumar;

	public Monedes(String nom) {
		super();
		this.nom = nom;
		this.puntssumar = 10;
	}

	@Override
	public String toString() {
		return "Monedes [nom=" + nom + ", puntssumar=" + puntssumar + "]";
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getPuntssumar() {
		return puntssumar;
	}

	public void setPuntssumar(int puntssumar) {
		this.puntssumar = puntssumar;
	}

	@Override
	public int getPunts() {
		
		return puntssumar;
	}

	@Override
	public int getpunts() {
		return 0 + this.puntssumar;
	}

}
