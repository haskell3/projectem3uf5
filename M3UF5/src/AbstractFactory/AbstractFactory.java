package AbstractFactory;

public interface  AbstractFactory<T> {
	T create(String nom,String tipus);

}
