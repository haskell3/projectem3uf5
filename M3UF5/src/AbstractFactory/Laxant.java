package AbstractFactory;

public class Laxant implements ObjecteMillorar {
	//El factor lo que hace es que al defenderte el tiene un debufo o al cazar un objeto tienes un bonus menos el laxante que te debufea a ti
	String nom;
	double factor;
	
	public Laxant(String nom) {
		super();
		this.nom = nom;
		this.factor = 2;
	}

	@Override
	public String toString() {
		return "Laxant [nom=" + nom + ", factor=" + factor + "]";
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public double getFactor() {
		return factor;
	}

	public void setFactor(double factor) {
		this.factor = factor;
	}
	


}
