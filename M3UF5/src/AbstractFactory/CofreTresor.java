package AbstractFactory;

import observer.ObjecteGuanyable;

public class CofreTresor implements ObjectesCacar, ObjecteGuanyable {
	String nom;
	int puntssumar;

	public CofreTresor(String nom) {
		super();
		this.nom = nom;
		this.puntssumar = 50;
	}

	@Override
	public String toString() {
		return "CofreTresor [nom=" + nom + ", puntssumar=" + puntssumar + "]";
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getPuntssumar() {
		return puntssumar;
	}

	public void setPuntssumar(int puntssumar) {
		this.puntssumar = puntssumar;
	}

	@Override
	public int getPunts() {
		
		return puntssumar;
	}
	
	@Override
	public int getpunts() {
		return 0 + this.puntssumar;
	}


}
