package AbstractFactory;

import observer.ObjecteGuanyable;

public class Bestia implements Enemic, ObjecteGuanyable {
	String nom;
	int puntsrestar;

	public Bestia(String nom) {
		super();
		this.nom = nom;
		this.puntsrestar = -7;
	}

	@Override
	public String toString() {
		return "Bestia [nom=" + nom + ", puntsrestar=" + puntsrestar + "]";
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getPuntsrestar() {
		return puntsrestar;
	}

	public void setPuntsrestar(int puntsrestar) {
		this.puntsrestar = puntsrestar;
	}

	@Override
	public int getPunts() {
		
		return puntsrestar;
	}

}
