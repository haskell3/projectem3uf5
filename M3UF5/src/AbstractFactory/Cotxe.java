package AbstractFactory;

public class Cotxe implements Vehicles{
	String nom;

	public Cotxe(String nom) {
		super();
		this.nom = nom;
	}

	@Override
	public String toString() {
		return "Cotxe [nom=" + nom + "]";
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

}
