package AbstractFactory;

public class Pistola implements ObjecteMillorar {
	//El factor lo que hace es que al defenderte el tiene un debufo o al cazar un objeto tienes un bonus menos el laxante que te debufea a ti
		String nom;
		double factor;
		
		public Pistola(String nom) {
			super();
			this.nom = nom;
			this.factor = 0.25;
		}

		@Override
		public String toString() {
			return "Pistola [nom=" + nom + ", factor=" + factor + "]";
		}

		public String getNom() {
			return nom;
		}

		public void setNom(String nom) {
			this.nom = nom;
		}

		public double getFactor() {
			return factor;
		}

		public void setFactor(double factor) {
			this.factor = factor;
		}
		

}
