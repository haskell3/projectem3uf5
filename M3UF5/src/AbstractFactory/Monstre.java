package AbstractFactory;

import FactoryMethod.Jugador;
import observer.ObjecteGuanyable;

public class Monstre implements Enemic {
	String nom;
	int puntsrestar;
	public Jugador jugador;

	public Monstre(String nom) {
		super();
		this.nom = nom;
		this.puntsrestar = -15;
	}

	@Override
	public String toString() {
		return "Monstre [nom=" + nom + ", puntsrestar=" + puntsrestar + "]";
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getPuntsrestar() {
		return puntsrestar;
	}

	public void setPuntsrestar(int puntsrestar) {
		this.puntsrestar = puntsrestar;
	}

	@Override
	public int getPunts() {

		return puntsrestar;
	}

}
