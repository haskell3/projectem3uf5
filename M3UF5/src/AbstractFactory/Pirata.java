package AbstractFactory;

import observer.ObjecteGuanyable;

public class Pirata implements Enemic, ObjecteGuanyable {
	String nom;
	int puntsrestar;

	public Pirata(String nom) {
		super();
		this.nom = nom;
		this.puntsrestar = -10;
	}

	@Override
	public String toString() {
		return "Pirata [nom=" + nom + ", puntsrestar=" + puntsrestar + "]";
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getPuntsrestar() {
		return puntsrestar;
	}

	public void setPuntsrestar(int puntsrestar) {
		this.puntsrestar = puntsrestar;
	}

	@Override
	public int getPunts() {
		
		return puntsrestar;
	}
	
	

}
