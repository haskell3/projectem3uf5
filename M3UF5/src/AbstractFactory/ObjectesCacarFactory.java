package AbstractFactory;

public class ObjectesCacarFactory implements AbstractFactory<ObjectesCacar> {

	@Override
	public ObjectesCacar create(String nom, String tipus) {
		if (tipus.equalsIgnoreCase("Monedes")) {
			return new Monedes(nom);
		} else if (tipus.equalsIgnoreCase("Lingot d'or")) {
			return new LingotDor(nom);
		} else if (tipus.equalsIgnoreCase("Cofre del tresor")) {
			return new CofreTresor(nom);
		} else {
			return null;
		}
	}

}
