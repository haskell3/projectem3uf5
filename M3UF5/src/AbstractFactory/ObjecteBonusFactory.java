package AbstractFactory;

public class ObjecteBonusFactory implements AbstractFactory<ObjecteBonus> {

	@Override
	public ObjecteBonus create(String nom, String tipus) {
		if (tipus.equalsIgnoreCase("Hamburguesa")) {
			return new Hamburguesa(nom);
		} else if (tipus.equalsIgnoreCase("Diamant")) {
			return new Diamant(nom);
		} else if (tipus.equalsIgnoreCase("Red Bull")) {
			return new RedBull(nom);
		} else {
			return null;
		}
	}

}
