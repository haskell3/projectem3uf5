package AbstractFactory;

public class ObjecteMillorarFactory implements AbstractFactory<ObjecteMillorar> {

	@Override
	public ObjecteMillorar create(String nom, String tipus) {
		if (tipus.equalsIgnoreCase("Espasa")) {
			return new Espasa(nom);
		} else if (tipus.equalsIgnoreCase("Pistola")) {
			return new Pistola(nom);
		} else if (tipus.equalsIgnoreCase("Laxant")) {
			return new Laxant(nom);
		} else {
			return null;
		}
	}

}
