package AbstractFactory;

public class FactoryProvider {
	
	public static AbstractFactory getFactory(String tipus) {
		if (tipus.equals("Enemic")) {
			return new EnemicFactory();
		} else if (tipus.equals("Objecte a caçar")) {
			return new ObjectesCacarFactory();
		} else if (tipus.equals("Objectes a millorar")) {
			return new ObjecteMillorarFactory();
		} else if (tipus.equals("Objectes bonus")) {
			return new ObjecteBonusFactory();
		} else if (tipus.equals("Vehicle")) {
			return new VehiclesFactory();
		} else {
			return null;
		}
		
	}

}
