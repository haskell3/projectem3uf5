package AbstractFactory;

public class AlaDelta implements Vehicles {
	String nom;

	public AlaDelta(String nom) {
		super();
		this.nom = nom;
	}

	@Override
	public String toString() {
		return "AlaDelta [nom=" + nom + "]";
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

}
