package AbstractFactory;

public class VehiclesFactory implements AbstractFactory<Vehicles> {

	@Override
	public Vehicles create(String nom, String tipus) {
		if (tipus.equalsIgnoreCase("Barca")) {
			return new Barca(nom);
		} else if (tipus.equalsIgnoreCase("Cotxe")) {
			return new Cotxe(nom);
		} else if (tipus.equalsIgnoreCase("Patinet")) {
			return new Patinet(nom);
		} else if (tipus.equalsIgnoreCase("Ala delta")) {
			return new AlaDelta(nom);
		} else {
			return null;
		}
	}

}
