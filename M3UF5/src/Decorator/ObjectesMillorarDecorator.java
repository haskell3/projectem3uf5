package Decorator;

import AbstractFactory.ObjecteMillorar;
import FactoryMethod.Jugador;
import FactoryMethod.TipusJugador;

public class ObjectesMillorarDecorator extends JugadorDecorator{
	private ObjecteMillorar objecte;
	
	
	public ObjectesMillorarDecorator(Jugador jugador, ObjecteMillorar objecte) {
		super(jugador);
		this.objecte = objecte;
		super.setObjecteMillorar(objecte);
	}
	
	@Override
	public String getNom() {
		return super.getNom();
	}

	@Override
	public int getPunts() {
		return super.getPunts();
	}

	@Override
	public double getSaldo() {
		return super.getSaldo();
	}

	@Override
	public TipusJugador getTipusJugador() {
		return super.getTipusJugador();
	}
	
	@Override
	public void descripcio() {
		super.descripcio();
		System.out.println("Objecte: " + objecte);
	}

	@Override
	public void setPunts(int i) {
		this.getJugador().setPunts(i);
		
	}

	@Override
	public void setObjecteMillorar(ObjecteMillorar o) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ObjecteMillorar getObjecteMillorar() {
		// TODO Auto-generated method stub
		return super.getJugador().getObjecteMillorar();
	}
	
	

}
