package Decorator;

import AbstractFactory.ObjecteMillorar;
import FactoryMethod.Jugador;
import FactoryMethod.TipusJugador;

public class ColorJugadorDecorator extends JugadorDecorator{
	private Color color;

	public ColorJugadorDecorator(Jugador jugador, Color color) {
		super(jugador);
		this.color = color;
	}
	
	@Override
	public String getNom() {
		return super.getNom();
	}

	@Override
	public int getPunts() {
		return super.getPunts();
	}

	@Override
	public double getSaldo() {
		return super.getSaldo();
	}

	@Override
	public TipusJugador getTipusJugador() {
		return super.getTipusJugador();
	}

	@Override
	public void descripcio() {
		super.descripcio();
		System.out.println("Color: " + color);
	}

	@Override
	public void setPunts(int i) {
		this.getJugador().setPunts(i);
		
	}

	@Override
	public void setObjecteMillorar(ObjecteMillorar obj) {
		
		
	}

	@Override
	public ObjecteMillorar getObjecteMillorar() {
		
		return null;
	}
	
	
	
}
