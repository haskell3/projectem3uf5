package Decorator;

import AbstractFactory.ObjecteMillorar;
import FactoryMethod.Jugador;
import FactoryMethod.TipusJugador;

public abstract class JugadorDecorator implements Jugador {
	private Jugador jugador;

	
	public JugadorDecorator(Jugador jugador) {
		super();
		this.jugador = jugador;
	}

	public String getNom() {
		return jugador.getNom();
	}

	public int getPunts() {
		return jugador.getPunts();
	}

	public double getSaldo() {
		return jugador.getSaldo();
	}

	public TipusJugador getTipusJugador() {
		return jugador.getTipusJugador();
	}
	
	@Override
	public void descripcio() {
		jugador.descripcio();
	}
	
	@Override
	public void setObjecteMillorar(ObjecteMillorar o) {
		jugador.setObjecteMillorar(o);
	}

	public Jugador getJugador() {
		return jugador;
	}

	@Override
	public String toString() {
		return "JugadorDecorator [jugador=" + jugador + "]";
	}
	
}
