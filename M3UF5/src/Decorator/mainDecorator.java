package Decorator;

import AbstractFactory.Cotxe;
import AbstractFactory.FactoryProvider;
import AbstractFactory.Pistola;
import FactoryMethod.Jugador;
import FactoryMethod.JugadorComerciant;
import FactoryMethod.JugadorFactory;
import FactoryMethod.JugadorGuerrer;
import FactoryMethod.JugadorMag;
import FactoryMethod.TipusJugador;

public class mainDecorator {
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		JugadorFactory ff = new JugadorFactory();
		JugadorComerciant comerciant = (JugadorComerciant) ff.crearJugador("Paco", 2, TipusJugador.Comerciant);
		JugadorGuerrer guerrer = (JugadorGuerrer) ff.crearJugador("Paca", 4, TipusJugador.Guerrer);
		JugadorMag mag = (JugadorMag) ff.crearJugador("Mariano Rajoy", 0.1, TipusJugador.Mag);
		Cotxe c = (Cotxe) FactoryProvider.getFactory("Vehicle").create("Sandero", "Cotxe");
		Pistola p = (Pistola) FactoryProvider.getFactory("Objectes a millorar").create("Si", "Pistola");

		Jugador test1 = comerciant;
		test1 = new ColorJugadorDecorator(test1, Color.AQUA);
		test1 = new VehiclesDecorator(test1, c);
		test1 = new ObjectesMillorarDecorator(test1, p);
		test1.descripcio();
		System.out.println();
		
		Jugador test2 = (JugadorGuerrer) ff.crearJugador("Paca", 4, TipusJugador.Guerrer);
		test2 = new ObjectesMillorarDecorator(test2, p);
		test2 = new VehiclesDecorator(test2, c);
		test2 = new ColorJugadorDecorator(test2, Color.AQUA);
		test2.descripcio();
		System.out.println();
		
		Jugador test3 = mag;
		test3 = new ColorJugadorDecorator(new VehiclesDecorator(new ObjectesMillorarDecorator(test3, p), c),
				Color.AQUA);
		test3.descripcio();

	}

}
