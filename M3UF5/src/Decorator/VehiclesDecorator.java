package Decorator;

import AbstractFactory.ObjecteMillorar;
import AbstractFactory.Vehicles;
import FactoryMethod.Jugador;
import FactoryMethod.TipusJugador;

public class VehiclesDecorator extends JugadorDecorator {
	private Vehicles vehicle;

	public VehiclesDecorator(Jugador jugador, Vehicles vehicle) {
		super(jugador);
		this.vehicle = vehicle;
	}

	@Override
	public String getNom() {
		return super.getNom();
	}

	@Override
	public int getPunts() {
		return super.getPunts();
	}

	@Override
	public double getSaldo() {
		return super.getSaldo();
	}

	@Override
	public TipusJugador getTipusJugador() {
		return super.getTipusJugador();
	}

	@Override
	public void descripcio() {
		super.descripcio();
		System.out.println("Vehicle: " + vehicle);
	}

	@Override
	public void setPunts(int i) {
		this.getJugador().setPunts(i);
		
	}

	@Override
	public void setObjecteMillorar(ObjecteMillorar o) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ObjecteMillorar getObjecteMillorar() {
		// TODO Auto-generated method stub
		return null;
	}
}
