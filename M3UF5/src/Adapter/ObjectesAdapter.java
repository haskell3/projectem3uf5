package Adapter;

import AbstractFactory.ObjecteBonus;
import AbstractFactory.ObjectesCacar;

public class ObjectesAdapter implements AdapterInterface{
	 ObjectesCacar obj;
	 ObjecteBonus obj2;
	 double factor;
	 
	 
	public ObjectesAdapter(ObjecteBonus e, double f) {
		super();
		this.obj2 = e;
		this.factor = f;
	}
	 
	public ObjectesAdapter(ObjectesCacar e, double f) {
		super();
		this.obj = e;
		this.factor = f;
	}



	@Override
	public int getPunts() {
		if(obj == null)
			return (int) (obj2.getpunts() / factor);
		else
			return (int) (obj.getpunts() / factor);
	}
	
}
