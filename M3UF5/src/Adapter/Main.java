package Adapter;

import AbstractFactory.FactoryProvider;
import AbstractFactory.Monstre;
import AbstractFactory.Pistola;
import Decorator.ObjectesMillorarDecorator;
import FactoryMethod.Jugador;
import FactoryMethod.JugadorComerciant;
import FactoryMethod.JugadorFactory;
import FactoryMethod.TipusJugador;
import observer.ColectorPunts;
import observer.ObjecteGuanyat;
import observer.TipusJugadorFactoryMethod;

public class Main {
	public static void main(String[] args) {
		ColectorPunts cPunts = new ColectorPunts();
		JugadorFactory f = new JugadorFactory();
		JugadorComerciant jc = (JugadorComerciant) f.crearJugador("Frodo", 10, TipusJugador.Comerciant);
		TipusJugadorFactoryMethod jcT = new TipusJugadorFactoryMethod(jc.getNom(), 0, cPunts);
		System.out.println(jcT);
		
		Pistola p = (Pistola) FactoryProvider.getFactory("Objectes a millorar").create("Si", "Pistola");

		Jugador j = jc;
		j =  new ObjectesMillorarDecorator(j, p);
		System.out.println(j.getObjecteMillorar().getFactor());
		
		
		Monstre m = (Monstre) FactoryProvider.getFactory("Enemic").create("Sauron", "Monstre");
		
		ObjecteGuanyat og = new ObjecteGuanyat(j, m);
		
		cPunts.cambienPuntuaciones(og);
		
		System.out.println(jcT);
		
		cPunts.seAcaboElJogo();
	}
}
