package Adapter;

import AbstractFactory.Enemic;

public class EnemicAdapter implements AdapterInterface {

	 Enemic e;
	 double factor;
	 
	 
	public EnemicAdapter(Enemic e, double f) {
		super();
		this.e = e;
		this.factor = f;
	}



	@Override
	public int getPunts() {
		return (int ) (e.getPunts() * factor);
	}
	
}
