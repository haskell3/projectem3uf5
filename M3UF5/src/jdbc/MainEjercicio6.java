package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;

import FactoryMethod.JugadorComerciant;
import FactoryMethod.JugadorFactory;
import FactoryMethod.JugadorGuerrer;
import FactoryMethod.TipusJugador;

public class MainEjercicio6 {
	
	public static void mostrarJugadors(Statement stmt, ResultSet rs) {
		
		try {
			if(stmt.execute("SELECT * FROM jugador"))
			{
				rs = stmt.getResultSet();
				while(rs.next())
				{
					System.out.println("Nom: " + rs.getString("nom") + ", saldo: " + rs.getDouble("saldo") + ", punts: " + rs.getDouble("punts"));
				}
				System.out.println();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	public static <T> void main(String[] args) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		ArrayList<JugadorComparable> jugadors = new ArrayList<JugadorComparable>();
		
		try {

			conn = DriverManager.getConnection("jdbc:mysql://localhost/videojocjdbc?" + "user=root&password=super3");

			stmt = conn.createStatement();
			
			//Sentencia para quitar el safe mode y poder updatear y deletear sin usar primary key(necesario para el delete por puntos)
			stmt.execute("SET SQL_SAFE_UPDATES = 0;");
			
			JugadorFactory f = new JugadorFactory();
			JugadorComerciant jc = (JugadorComerciant) f.crearJugador("Frodo", 10, TipusJugador.Comerciant);
			JugadorGuerrer jg = (JugadorGuerrer) f.crearJugador("Sam", 15, TipusJugador.Guerrer);
			jugadors.add(new JugadorComparable(jc));
			jugadors.add(new JugadorComparable(jg));
			
			for (JugadorComparable jugador : jugadors) {
				stmt.execute("insert into jugador (nom, saldo, punts) values ('" + jugador.j.getNom() + "', " + jugador.j.getSaldo() + ", " + jugador.j.getPunts() + ")");
			}

			System.out.println("---Jugadors abans de la partida---");
			mostrarJugadors(stmt, rs);

			jc.setPunts(600);
			jg.setPunts(700);
			
			for (JugadorComparable jugador : jugadors) {
				//Se que la clave primaria no es el nombre pero la clase jugador no tiene id asique que para mi el nombre es clave primaria :)
				//Al que no le vaya esta sentendia antes tiene que ejecutar esto: SET SQL_SAFE_UPDATES = 0;
				//Es para quitar el safe mode que te impide update or delete de querys que no tengan un where con clave primaria
				//La sentencia es necesaria anque este update se hiciese con primary key debido al delete que viene despues
				//Ya he ejecutado yo la sentencia al principio del codigo pero no borro este comentario por pereza
				
				//Vaya pringado el de los comentarios de arriba ^
				stmt.execute("update jugador set punts = " + jugador.j.getPunts() + " where nom = '" + jugador.j.getNom() + "'");
			}
			System.out.println("---Jugadors amb punts updatejats---");
			mostrarJugadors(stmt, rs);
			
			System.out.println("Borrem els que no arriban a 500 punts.");
			stmt.execute("delete from jugador where punts < 500");
			
			mostrarJugadors(stmt, rs);
			
			System.out.println("Llista ordenada per noms ascendent");
			
			Collections.sort(jugadors);
			System.out.println(jugadors);
			
			System.out.println("Llista ordenada per punts descendents");
			
			Collections.sort(jugadors, new ComparatorPunts());
			System.out.println(jugadors);
			
		} catch (SQLException ex) {

			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());

		} finally {

			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
				
				} 
				rs = null;
			}
			if (stmt != null) {

				try {

					stmt.close();
				} catch (SQLException sqlEx) {
					
				} 
				stmt = null;
			}
		}

	}
}
