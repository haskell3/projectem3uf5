package jdbc;

import java.util.Comparator;

import FactoryMethod.Jugador;

public class ComparatorPunts implements Comparator<JugadorComparable>{

	public ComparatorPunts() {
		super();
	}
	@Override
	public int compare(JugadorComparable o1, JugadorComparable o2) {
		return Double.compare(o2.j.getPunts(), o1.j.getPunts());
	}

}
