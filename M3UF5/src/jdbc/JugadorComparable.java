package jdbc;

import FactoryMethod.Jugador;

public class JugadorComparable implements Comparable<JugadorComparable>{

	public Jugador j;

	public JugadorComparable(Jugador j) {
		super();
		this.j = j;
	}

	@Override
	public int compareTo(JugadorComparable o) {
		return this.j.getNom().compareToIgnoreCase(o.j.getNom());
	}

	@Override
	public String toString() {
		return j.toString();
	}
	
}
