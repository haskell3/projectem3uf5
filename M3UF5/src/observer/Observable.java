package observer;

public interface Observable {
	
	public void addJugador(Observador o);
	public void removeJugador(Observador o);
	public void cambienPuntuaciones(ObjecteGuanyat og);
	public void seAcaboElJogo();

}
