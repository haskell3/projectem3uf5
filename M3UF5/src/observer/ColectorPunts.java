package observer;

import java.util.ArrayList;

public class ColectorPunts implements Observable{
	
	private ArrayList<Observador> jugadors = new ArrayList<Observador>();
	
	public ColectorPunts() {
		super();
	}

	@Override
	public void addJugador(Observador o) {
		
		if(!jugadors.contains(o))
			jugadors.add(o);
	}

	@Override
	public void removeJugador(Observador o) {
		
		if(jugadors.contains(o))
			jugadors.remove(o);
	}

	@Override
	public void cambienPuntuaciones(ObjecteGuanyat og) {
		
		for (Observador observador : jugadors) {
			observador.setPunts(og);
		}
	}

	@Override
	public void seAcaboElJogo() {	
		/*for (Observador observador : jugadors) {
			observador.posMeVoy(this);
		}*/
		while(!jugadors.isEmpty()) {
			jugadors.get(0).posMeVoy(this);
		}
	}
	
	/* Alternativa muy eficaz pero como no seria observador observable alomejor es terrorista
	 * public void seAcaboElJogo(){
	 * 	jugadors.clear();
	 * }
	 */
}
