package observer;

import AbstractFactory.Enemic;
import AbstractFactory.ObjecteBonus;
import AbstractFactory.ObjectesCacar;
import Adapter.EnemicAdapter;
import Adapter.ObjectesAdapter;

public class TipusJugadorFactoryMethod implements Observador{
	String nom;
	int punts;
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public int getPunts() {
		return this.punts;
	}
	public void setPunts(int punts) {
		this.punts = punts;
	}
	public TipusJugadorFactoryMethod(String nom, int punts) {
		super();
		this.nom = nom;
		this.punts = punts;
	}
	public TipusJugadorFactoryMethod(String nom, int punts, Observable o) {
		super();
		this.nom = nom;
		this.punts = punts;
		o.addJugador(this);
	}
	@Override
	public String toString() {
		return "TipusJugadorFactoryMethod [nom=" + nom + ", punts=" + punts + "]";
	}
	@Override
	public void posMeVoy(Observable o) {
		
		o.removeJugador(this);
	}
	@Override
	public void setPunts(ObjecteGuanyat og) {
		
		if(this.nom.equals(og.j.getNom())) {
			if (og.j.getObjecteMillorar() != null) {
				if(og.og instanceof Enemic) {
					EnemicAdapter e = new EnemicAdapter((Enemic)og.og, og.j.getObjecteMillorar().getFactor());
					this.punts += e.getPunts();
				}else if(og.og instanceof ObjectesCacar) {
					ObjectesAdapter e = new ObjectesAdapter((ObjectesCacar)og.og, og.j.getObjecteMillorar().getFactor());
					this.punts += e.getPunts();
				}else if(og.og instanceof ObjecteBonus) {
					ObjectesAdapter e = new ObjectesAdapter((ObjecteBonus)og.og, og.j.getObjecteMillorar().getFactor());
					this.punts += e.getPunts();
				}
			} else {
				if(og.og instanceof Enemic) {
					EnemicAdapter e = new EnemicAdapter((Enemic)og.og, 1);
					this.punts += e.getPunts();
				}else if(og.og instanceof ObjectesCacar) {
					ObjectesAdapter e = new ObjectesAdapter((ObjectesCacar)og.og, 1);
					this.punts += e.getPunts();
				}else if(og.og instanceof ObjecteBonus) {
					ObjectesAdapter e = new ObjectesAdapter((ObjecteBonus)og.og, 1);
					this.punts += e.getPunts();
				}
			}
			
			
		}
		
	}
	
}
