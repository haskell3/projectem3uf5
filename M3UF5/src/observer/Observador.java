package observer;

public interface Observador {
	public void setPunts(ObjecteGuanyat og);
	public void posMeVoy(Observable o);
}
