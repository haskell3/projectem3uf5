package observer;

import AbstractFactory.FactoryProvider;
import AbstractFactory.Monstre;
import FactoryMethod.JugadorComerciant;
import FactoryMethod.JugadorFactory;
import FactoryMethod.TipusJugador;

public class MainObserver {

	public static void main(String[] args) {
		ColectorPunts cPunts = new ColectorPunts();
		JugadorFactory f = new JugadorFactory();
		JugadorComerciant jc = (JugadorComerciant) f.crearJugador("Frodo", 10, TipusJugador.Comerciant);
		TipusJugadorFactoryMethod jcT = new TipusJugadorFactoryMethod(jc.getNom(), 0, cPunts);
		System.out.println(jcT);
		
		Monstre m = (Monstre) FactoryProvider.getFactory("Enemic").create("Sauron", "Monstre");
		
		ObjecteGuanyat og = new ObjecteGuanyat(jc, m);
		
		cPunts.cambienPuntuaciones(og);
		
		System.out.println(jcT);
		
		cPunts.seAcaboElJogo();
		
	}

}
