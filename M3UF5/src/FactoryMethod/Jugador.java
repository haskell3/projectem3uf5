package FactoryMethod;

import AbstractFactory.ObjecteMillorar;
import observer.Observador;

public interface Jugador {
	
	public ObjecteMillorar getObjecteMillorar();
	public void setObjecteMillorar(ObjecteMillorar o);
	public String getNom();
	public int getPunts();
	public double getSaldo();
	public TipusJugador getTipusJugador();
	public void descripcio();
	public void setPunts(int i);
}
