package FactoryMethod;

import AbstractFactory.Cotxe;
import AbstractFactory.FactoryProvider;
import AbstractFactory.Pistola;

public class main {

	public static void main(String[] args) {
		JugadorFactory ff = new JugadorFactory();
		JugadorComerciant comerciant = (JugadorComerciant) ff.crearJugador("Paco", 2, TipusJugador.Comerciant);
		JugadorGuerrer guerrer = (JugadorGuerrer) ff.crearJugador("Paca", 4, TipusJugador.Guerrer);
		JugadorMag mag = (JugadorMag) ff.crearJugador("Mariano Rajoy", 0.1, TipusJugador.Mag);
		Cotxe c = (Cotxe) FactoryProvider.getFactory("Vehicle").create("Sandero", "Cotxe");
		Pistola p = (Pistola) FactoryProvider.getFactory("Objectes a millorar").create("Si", "Pistola");
		
		System.out.println(comerciant.toString());
		System.out.println(guerrer.toString());
		System.out.println(mag.toString());

	}

}
