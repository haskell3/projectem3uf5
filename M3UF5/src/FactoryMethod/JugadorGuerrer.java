package FactoryMethod;

import AbstractFactory.ObjecteMillorar;

public class JugadorGuerrer implements Jugador{

	String nom;
	int punts;
	double saldo;
	TipusJugador tipus;
	public ObjecteMillorar obj;

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getPunts() {
		return punts;
	}

	public void setPunts(int punts) {
		this.punts = punts;
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public TipusJugador getTipusJugador() {
		return tipus;
	}

	public void setTipus(TipusJugador tipus) {
		this.tipus = tipus;
	}
	
	public JugadorGuerrer(String nom, int punts, double saldo) {
		this.nom = nom;
		this.punts = punts;
		this.saldo = saldo;
		this.tipus = TipusJugador.Guerrer;
	}

	@Override
	public String toString() {
		return "JugadorGuerrer [nom=" + nom + ", punts=" + punts + ", saldo=" + saldo + ", tipus=" + tipus + "]";
	}
	
	@Override
	public void descripcio() {
		System.out.println(this.toString());
	}

	@Override
	public void setObjecteMillorar(ObjecteMillorar obj) {
		
		this.obj = obj;
	}

	@Override
	public ObjecteMillorar getObjecteMillorar() {
		// TODO Auto-generated method stub
		return obj;
	}


}
