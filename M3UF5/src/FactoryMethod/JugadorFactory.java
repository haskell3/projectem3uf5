package FactoryMethod;

public class JugadorFactory {
	
	public Jugador crearJugador(String nom, double saldo, TipusJugador tipus) {
		if (tipus == TipusJugador.Comerciant) {
			return new JugadorComerciant(nom, 0, saldo);
		} else if (tipus == TipusJugador.Guerrer) {
			return new JugadorGuerrer(nom, 0, saldo);
		} else if (tipus == TipusJugador.Mag) {
			return new JugadorMag(nom, 0, saldo);
		} else {
			return null;
		}
	}

}
